type Api {
  repository(url: String): Repository
}

type Digests {
  md5: String
  sha256: String
}

type Downloads {
  last_day: Int
  last_month: Int
  last_week: Int
}

type Info {
  summary: String
  description: String
  description_content_type: String
  keywords: String
  home_page: String
  version: String
  name: String
  author: String
  author_email: String
  maintainer: String
  maintainer_email: String
  license: String
  platform: String
  download_url: String
  release_url: String
  project_url: String
  docs_url: String
  bugtrack_url: String
  classifiers: [String]
  requires_dist: [String]
  requires_python: String
  downloads: Downloads
}

type Item {
  title: String
  guid: String
  category: [String]
  link: String
  description: String
  pubDate: String
  location: String
}

type Package {
  info: Info
  versions: [Version]
}

type Query {
  pypi: Api
}

type Release {
  comment_text: String
  digests: Digests
  downloads: Int
  filename: String
  has_sig: Boolean
  md5_digest: String
  packagetype: String
  python_version: String
  size: Int
  upload_time: String
  url: String
}

type Feed {
  name: String
  href: String
}

type Repository {
  url: String
  feeds: [Feed]
  package(name: String): Package
}

type Version {
  version: String
  releases: [Release]
}

schema {
  query: Query
}
